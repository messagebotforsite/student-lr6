import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map, delay} from "rxjs/operators";

export interface Todo {
  id?: number;
  title: string;
  completed: boolean;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  readonly apiUrl = 'https://jsonplaceholder.typicode.com';

  posts: Todo[] = [];
  namePost = '';
  flagLoad = false;
  
  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.loadPost();
  }

  loadPost() {
    this.flagLoad = true;
    this.http.get<Todo[]>(`${this.apiUrl}/todos?_limit=5`)
      .pipe(delay(1500))
      .subscribe(response => {
        console.log(response)
        this.posts = response;
        this.flagLoad = false;
      });
  }

  addPost() {
    console.log(this.namePost)
    if (!this.namePost.trim()) {
      return;
    }

    const post: Todo = {
      title: this.namePost,
      completed: false
    };

    this.http.post<Todo>(`${this.apiUrl}/todos`, post)
      .subscribe(newPost => {
        console.log(newPost);
        this.posts.unshift(newPost);
      });
  }

  removePost(id: number) {
    this.http.delete(`${this.apiUrl}/todos/${id}`)
      .subscribe(() => {
        console.log(id);
        this.posts = this.posts.filter(item => item.id != id);
        console.log(this.posts);
      });
  }

  completedPost(id: number) {
    return this.http.put<Todo>(`${this.apiUrl}/todos/${id}`, {completed: true})
      .subscribe((res) => {
        this.posts.find(item => item.id === res.id).completed = true;
      });
  }
}
